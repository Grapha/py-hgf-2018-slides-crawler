# Hyperledger Global Forum 2018 - The conference slides crawler


## 1. Overview

For whom have went to the **Hyperledger Global Forum** [event](https://www.hyperledger.org/event/hyperledger-global-forum) organized into Basel in Switzerland between the **12th and 15th December 2018**.

Slides of conferences have been release by attendees in the agenda website [here](https://hgf18.sched.com/) right after the event.

*Note:* In the case where the **agenda** is not online anymore, you can find into the repo the following files giving shorten and detailed view of the agenda.

 - *hgf-agenda-detailed.pdf*
 - *hgf-agenda-simple.pdf*


## 2. Purpose

The purpose is to automatize through a Python3 script to download all the slide files provided by each page of the agenda, when slides are available.

**Two Phases:**

 - Locate slide links by crawling the agenda website using **Scrapy**.
 - Download slides into files and store them into a folder hierarchy.

## 3. Installation

### Prerequisites

Install **pip3**.

	$ sudo apt install python3-pip

Upgrade **pip3** (if needed).

	$ sudo pip3 install --upgrade pip

### Scrapy

Installing [Scrapy](https://scrapy.org/) a python web crawler.

	$ pip3 install scrapy

### Dependencies

 - [pathlib](https://docs.python.org/3/library/pathlib.html)
 - [urllib.request](https://docs.python.org/3/library/urllib.request.html)
 - [json](https://docs.python.org/3/library/json.html)


## 4. Usages

	$ git clone https://framagit.org/Grapha/py-hgf-2018-slides-crawler.git
	$ cd py-hgf-2018-slides-crawler
	$ git checkout latest

### Running the Scrapy spider

Crawling agenda in order to download slide links into the 'out.json' file.

	$ scrapy runspider my_spider.py -o out.json

### Downloading the files

Downloading slide files from link in 'out.json', and making folder hierarchy to store them.

	$ python3 slides_downloader.py

### Results

The results are in the following folder.

	$ ls ./slides/



