#!/usr/bin/env python
# coding: utf-8

# win> pip install scrapy
# unix> pip3 install scrapy
import scrapy



class MainSpider(scrapy.Spider):
	name = 'mainspider'
	start_urls = ['https://hgf18.sched.com/']
	ROOT_URL = 'https://hgf18.sched.com'

	def parse(self, response):
		return self.parse_mainpage(response) 

	def parse_mainpage(self, response):
		link_elmnts = response.xpath('//div[@class="sched-container"]/div[@class="sched-container-inner"]/span/a')
		links = link_elmnts.xpath("//a[contains(@href, '/event/')]/@href").extract()
		for link in links:
			# print(link)
			url = self.ROOT_URL+str(link)
			print("+-->\t"+str(url))
			req = scrapy.Request(url=url, callback=self.parse_eventpage)
			req.meta['url'] = url
			yield req
			# break
	
	def parse_eventpage(self, response):
		date = response.xpath('//div[@class="sched-container-header"]/@id').extract_first()
		name = response.xpath('//div[@class="sched-container sched-container-wide"]/div/span/a/text()').extract_first()
		slides = response.xpath('//div[@class="sched-file"]/a/@href')
		if len(slides) <= 0:
			# no slides
			return None
		return {
			'date': date,
			'name': name,
			'link': response.meta['url'],
			'slide_links': slides.extract()
		}




