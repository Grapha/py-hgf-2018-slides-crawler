#!/usr/bin/env python
# coding: utf-8

import json
import sys, os
import pathlib
import urllib.request


MAX_FOLDER_LENGTH = 55
filepath = "out.json"
root_path = "./slides"
print("> Loading crawler outcomes "+str(filepath)+" in order to download slides...")
with open(filepath, "r") as f:
	d = json.loads(f.read())
	print("#"+str(len(d)))
	
	for e in d:
		# no slides ?
		if len(e['slide_links']) <= 0:
			continue
		
		# making folder
		folder = str(e['link']).split("/")[-1]
		folder = folder[:MAX_FOLDER_LENGTH] if len(folder) >= MAX_FOLDER_LENGTH else folder
		path = str(root_path+"/"+e['date']+"/"+str(folder))
		r = pathlib.Path(path).mkdir(parents=True, exist_ok=True)
		
		# getting slides
		for slide in e['slide_links']:
			ext = str(slide).split(".")[-1]
			try:
				urllib.request.urlretrieve(slide, path+"/"+"slides."+ext)
				urllib.request.urlcleanup()
			except Exception as e:
				print(e)
		print(".", end="", flush=True)
	print("\n[+] Finished.")





